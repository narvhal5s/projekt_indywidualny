package pl.pw.edu.demo.services;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.pw.edu.demo.dto.requests.ChoreRequest;
import pl.pw.edu.demo.enums.ChoreType;
import pl.pw.edu.demo.enums.Difficulty;
import pl.pw.edu.demo.mappers.ChoreMapper;
import pl.pw.edu.demo.mappers.UserChoreMapper;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import javax.persistence.EntityManager;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
class ChoreRepositoryServiceTest {

    @Mock
    private ChoreRepository choreRepository;
    @Mock
    private ChoreMapper choreMapper;
    @Mock
    private UserChoreRepository userChoreRepository;
    @Mock
    private EntityManager em;
    @Mock
    private UserChoreMapper userChoreMapper;

    private ChoreRepositoryService choreRepositoryService;

    @BeforeEach
    void initUseCase() {
        choreRepositoryService = new ChoreRepositoryService(choreRepository,choreMapper,userChoreRepository,em,userChoreMapper);
    }

    @Test
    void saveChore(){
        //Given
        ChoreRequest request = new ChoreRequest("Name",15D, Difficulty.VERYHARD, ChoreType.PHYSICAL,"some description");

       choreRepositoryService.saveChore(request);
    }


    @Test
    void putUserChoreForNotExistingChore(){
        when(userChoreRepository.findById(1L)).thenReturn(Optional.empty());

        int result = choreRepositoryService.putUserChore(1L,100,100);

        assertEquals(404,result);
    }


}