package pl.pw.edu.demo.dto.requests;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import pl.pw.edu.demo.enums.ChoreType;
import pl.pw.edu.demo.enums.Difficulty;

@Data
@AllArgsConstructor
public class ChoreRequest {

    @ApiModelProperty(required = true)
    private String name;

    @ApiModelProperty(required = true)
    private Double time;

    @ApiModelProperty(required = true)
    private Difficulty difficult;

    @ApiModelProperty(required = true)
    private ChoreType type;

    @ApiModelProperty(required = true)
    private String details;

}
