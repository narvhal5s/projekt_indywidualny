package pl.pw.edu.demo.dto.requests;


import lombok.Getter;
import lombok.Setter;

import java.util.Set;
import javax.validation.constraints.*;

@Getter
@Setter
public class SignUpForm {
    @NotBlank
    @Size(min = 5, max = 50)
    private String name;

    @NotBlank
    @Size(min = 5, max = 50)
    private String username;

    @NotBlank
    @Size(max = 60)
    @Email
    private String email;

    private Set<String> role;

    @NotBlank
    @Size(min = 6, max = 40)
    private String password;

    private String sex;
    private Integer age;
}
