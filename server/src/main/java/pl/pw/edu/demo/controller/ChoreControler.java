package pl.pw.edu.demo.controller;

import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import pl.pw.edu.demo.dto.requests.ChoreRequest;
import pl.pw.edu.demo.dto.requests.UserChoreDetailRequest;
import pl.pw.edu.demo.dto.requests.UserChoreRequest;
import pl.pw.edu.demo.dto.responses.ResponseMessage;
import pl.pw.edu.demo.dto.snapshots.ChoreSnapshot;
import pl.pw.edu.demo.dto.snapshots.UserChoresSnapshot;
import pl.pw.edu.demo.entity.Chore;
import pl.pw.edu.demo.entity.User;
import pl.pw.edu.demo.entity.UserChore;
import pl.pw.edu.demo.services.*;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin
@Log
@RequestMapping("/api/chore")
public class ChoreControler {

    @Autowired
    private SecurityServices securityServices;

    @Autowired
    private ChoreService choreService;

    @Autowired
    private UserChoreRepository userChoreRepository;

    @Autowired
    private UserRepository userRepository;

    @PostMapping("/newchore")
    public int saveChore(@RequestBody ChoreRequest request){
        log.info("New post request" + request.toString());
        choreService.saveChore(request);
        return 0;
    }

    @GetMapping("/{id}")
    public  ResponseEntity<ChoreSnapshot> getChore( @PathVariable String id){
        log.info("Getting one chore by ID");
        return new ResponseEntity<>(choreService.getChore(Long.parseLong(id)),HttpStatus.OK);
    }

    @GetMapping("/randomchore")
    public ResponseEntity<Long> getRandomChoreId(){
        log.info("Getting one random chore id");
        return new ResponseEntity<>(1L,HttpStatus.OK);
    }

    @GetMapping("/allchore")
    public ResponseEntity<List<ChoreSnapshot>> getAllChores(){
        List<ChoreSnapshot> chores = choreService.getAllChores();
        return new ResponseEntity<>(chores, HttpStatus.OK);
    }

    @PostMapping("/userchore")
    public ResponseEntity<?> addUserChore(@RequestBody UserChoreRequest request){
        log.info("Adding user chore chore_id: " + request.getChoreID());
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        log.info("Adding chore for user: " + currentPrincipalName);
        UserChore userChore = new UserChore();
        Optional<Chore> chore = choreService.getChoreById(request.getChoreID());
        if(chore.isPresent()){
            userChore.setChore(chore.get());
        }else{
            return new ResponseEntity<>(new ResponseMessage("Nie znalezniono szukanego zadania"),HttpStatus.BAD_REQUEST);
        }
        String username = securityServices.findLoggedInUsername();
        User user = userRepository.findByUsername(currentPrincipalName).get();
        userChore.setUser(user);
        userChore.setSurveyStatus(false);
        userChoreRepository.save(userChore);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping("/userchore/{id}")
    public  ResponseEntity<UserChoresSnapshot> getUserChore( @PathVariable String id) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        User user = userRepository.findByUsername(currentPrincipalName).get();
        log.info("Getting one user (" + user.getUsername() + ") chore by ID" + id);
        return new ResponseEntity<>(choreService.getUserChore(Long.parseLong(id)), HttpStatus.OK);
    }

    @GetMapping("/userchore")
    public ResponseEntity<List<UserChoresSnapshot>> getAllUserChores(){
        log.info("Getting all user chore");
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        User user = userRepository.findByUsername(currentPrincipalName).get();
        return new ResponseEntity<>(choreService.getUserChores(user),HttpStatus.OK);
    }

    @PutMapping("/putUserChore")
    public ResponseEntity<?> putUserChoreDetail(@RequestBody UserChoreDetailRequest request){
       int status = choreService.putUserChore(request.getId(),request.getDone(),request.getFit());
       if(status==200){
           return new ResponseEntity<>(HttpStatus.OK);
       }else
           return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

}
