package pl.pw.edu.demo.services;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;
import pl.pw.edu.demo.entity.UserChore;

@Repository
@CrossOrigin
public interface UserChoreRepository extends JpaRepository<UserChore, Long> {
}
