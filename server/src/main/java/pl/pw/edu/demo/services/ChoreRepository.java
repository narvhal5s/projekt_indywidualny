package pl.pw.edu.demo.services;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;
import pl.pw.edu.demo.entity.Chore;

@Repository
@CrossOrigin
public interface ChoreRepository extends JpaRepository<Chore, Long> {
}
