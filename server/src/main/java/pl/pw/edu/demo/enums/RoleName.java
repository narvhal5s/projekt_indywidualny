package pl.pw.edu.demo.enums;

public enum RoleName {
    ROLE_USER,
    ROLE_PM,
    ROLE_ADMIN
}