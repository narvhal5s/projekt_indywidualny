package pl.pw.edu.demo.mappers;

import org.springframework.stereotype.Component;
import pl.pw.edu.demo.dto.snapshots.ChoreSnapshot;
import pl.pw.edu.demo.entity.Chore;

import java.util.ArrayList;
import java.util.List;

@Component
public class ChoreMapper {
    public List<ChoreSnapshot> toSnapshots(List<Chore> chores) {

        List<ChoreSnapshot> snapshots = new ArrayList<>();

        for (Chore chore : chores) {
            snapshots.add(toSnapshot(chore));
        }
        return snapshots;
    }

    public ChoreSnapshot toSnapshot(Chore chore) {
        return new ChoreSnapshot(chore.getId(), chore.getName(), chore.getTime(), chore.translateDifficult(), chore.translateChoreType(), chore.getDetails());
    }

}
