package pl.pw.edu.demo.enums;

public enum Difficulty {
    VERYEASY, EASY, MEDIUM, HARD, VERYHARD
}
