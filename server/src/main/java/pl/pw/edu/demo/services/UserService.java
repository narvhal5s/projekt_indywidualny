package pl.pw.edu.demo.services;

import org.springframework.stereotype.Service;
import pl.pw.edu.demo.entity.User;

import java.util.Optional;

@Service
public interface UserService {
    void save(User user);

    Optional<User> findByUsername(String username);
}
