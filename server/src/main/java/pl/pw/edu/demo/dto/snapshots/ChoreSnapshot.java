package pl.pw.edu.demo.dto.snapshots;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

@Data
@Getter
@AllArgsConstructor
public class ChoreSnapshot {
    private Long id;
    private String name;
    private Double time;
    private String difficulty;
    private String type;
    private String details;
}
