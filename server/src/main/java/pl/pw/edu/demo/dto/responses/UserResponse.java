package pl.pw.edu.demo.dto.responses;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class UserResponse {

    private String username;
    private String email;
    private String sex;
    private Integer age;
}
