package pl.pw.edu.demo.dto.requests;


import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
public class UserChoreRequest {
    @NotBlank
    private Long choreID;
}
