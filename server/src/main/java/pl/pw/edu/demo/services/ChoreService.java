package pl.pw.edu.demo.services;

import pl.pw.edu.demo.dto.requests.ChoreRequest;
import pl.pw.edu.demo.dto.snapshots.ChoreSnapshot;
import pl.pw.edu.demo.dto.snapshots.UserChoresSnapshot;
import pl.pw.edu.demo.entity.Chore;
import pl.pw.edu.demo.entity.User;

import java.util.List;
import java.util.Optional;

public interface ChoreService {
    List<UserChoresSnapshot> getUserChores(User user);

    void saveChore(ChoreRequest request);

    List<ChoreSnapshot> getAllChores();

    Long getRandomChore();

    Optional<Chore> getChoreById(Long choreID);

    ChoreSnapshot getChore(long l);

    UserChoresSnapshot getUserChore(long parseLong);

    int putUserChore(Long id, Integer done, Integer fit);
}
