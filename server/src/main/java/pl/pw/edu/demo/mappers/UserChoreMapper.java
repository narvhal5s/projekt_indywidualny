package pl.pw.edu.demo.mappers;

import org.springframework.stereotype.Component;
import pl.pw.edu.demo.dto.snapshots.UserChoresSnapshot;
import pl.pw.edu.demo.entity.UserChore;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserChoreMapper {

    public List<UserChoresSnapshot> toChoreSnapshot(List<UserChore> userChores) {
        List<UserChoresSnapshot> result = new ArrayList<>();

        for (UserChore chore : userChores) {
            result.add(toSnapshot(chore));
        }
        return result;
    }

    public UserChoresSnapshot toSnapshot(UserChore chore) {
        return new UserChoresSnapshot(
                chore.getId(),
                chore.getChore().getName(),
                chore.getChore().getTime(),
                chore.getChore().translateDifficult(),
                chore.getChore().translateChoreType(),
                chore.getChore().getDetails(),
                chore.isSurveyStatus(),
                chore.getFit(),
                chore.getDone()
        );
    }

}
