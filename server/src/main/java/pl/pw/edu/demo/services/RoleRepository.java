package pl.pw.edu.demo.services;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.CrossOrigin;
import pl.pw.edu.demo.entity.Role;
import pl.pw.edu.demo.enums.RoleName;

import java.util.Optional;

@Repository
@CrossOrigin
public interface RoleRepository extends JpaRepository<Role, Long> {
    Optional<Role> findByName(RoleName roleName);
}
