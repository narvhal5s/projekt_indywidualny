package pl.pw.edu.demo.dto.requests;

import lombok.Getter;

import javax.validation.constraints.NotNull;

@Getter
public class UserChoreDetailRequest {

    @NotNull
    Long id;

    @NotNull
    Integer done;

    @NotNull
    Integer fit;

}
