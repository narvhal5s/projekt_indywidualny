package pl.pw.edu.demo.services;

import lombok.AllArgsConstructor;
import lombok.extern.java.Log;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;
import pl.pw.edu.demo.dto.requests.ChoreRequest;
import pl.pw.edu.demo.dto.snapshots.ChoreSnapshot;
import pl.pw.edu.demo.dto.snapshots.UserChoresSnapshot;
import pl.pw.edu.demo.entity.Chore;
import pl.pw.edu.demo.entity.User;
import pl.pw.edu.demo.entity.UserChore;
import pl.pw.edu.demo.mappers.ChoreMapper;
import pl.pw.edu.demo.mappers.UserChoreMapper;

import javax.persistence.EntityManager;
import java.util.List;
import java.util.Optional;


@AllArgsConstructor
@Log
@Service
public class ChoreRepositoryService implements ChoreService {
    private final ChoreRepository choreRepository;
    private final ChoreMapper choreMapper;
    private final UserChoreRepository userChoreRepository;
    private final EntityManager em;
    private final UserChoreMapper userChoreMapper;

    @Override
    public void saveChore(ChoreRequest request) {
        log.info(request.toString());
        log.info("Save Chore:" + request.getName());
        Chore choreToSave = new Chore(0L, request.getName(), request.getTime(), request.getDifficult(), request.getType(), request.getDetails());
        choreRepository.save(choreToSave);
    }

    @Override
    public Long getRandomChore() {
        log.info("Getting random chore");
        Long id = 1L;
        return id;
    }

    @Override
    public List<ChoreSnapshot> getAllChores() {
        log.info("Getting all chores");
        List<Chore> chores = choreRepository.findAll();
        List<ChoreSnapshot> choreSnapshots = choreMapper.toSnapshots(chores);
        return choreSnapshots;
    }

    @Override
    public Optional<Chore> getChoreById(Long choreID) {
        return choreRepository.findById(choreID);
    }

    @Override
    public ChoreSnapshot getChore(long id) {
        return choreMapper.toSnapshot(getChoreById(id).get());
    }

    @Override
    public List<UserChoresSnapshot> getUserChores(User user) {
        Session session = em.unwrap(Session.class);
        Criteria criteria = session.createCriteria(UserChore.class);
        List<UserChore> list = criteria.add(Restrictions.eq("user", user)).list();
        return userChoreMapper.toChoreSnapshot(list);
    }

    @Override
    public UserChoresSnapshot getUserChore(long parseLong) {
        return userChoreMapper.toSnapshot(userChoreRepository.findById(parseLong).get());
    }

    @Override
    public int putUserChore(Long id, Integer done, Integer fit) {
        if (userChoreRepository.findById(id).isPresent()) {
            UserChore chore = userChoreRepository.findById(id).get();
            chore.setDone(done);
            chore.setFit(fit);
            chore.setSurveyStatus(true);
            userChoreRepository.save(chore);
            return 200;
        }
        return 404;
    }
}
