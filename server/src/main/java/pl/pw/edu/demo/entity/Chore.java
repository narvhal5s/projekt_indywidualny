package pl.pw.edu.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import pl.pw.edu.demo.enums.ChoreType;
import pl.pw.edu.demo.enums.Difficulty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Chore {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private String name;

    @NotNull
    private Double time;

    @NotNull
    private Difficulty difficult;

    @NotNull
    private ChoreType type;

    @NotNull
    private String details;


    public String translateDifficult() {
        Difficulty value = getDifficult();
        if (value == Difficulty.VERYEASY) {
            return "Bardzo łatwe";
        } else if (value == Difficulty.EASY) {
            return "Łatwe";
        } else if (value == Difficulty.MEDIUM) {
            return "Średnie";
        } else if (value == Difficulty.HARD) {
            return "Trudne";
        }
        return "Bardzo trudnw";
    }

    public String translateChoreType() {
        ChoreType value = getType();
        if (value == ChoreType.MENTAL) {
            return "Umysłowe";
        } else
            return "Fizyczne";
    }

}
