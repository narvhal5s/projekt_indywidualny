package pl.pw.edu.demo.dto.responses;

import lombok.AllArgsConstructor;
import lombok.Getter;
import pl.pw.edu.demo.dto.snapshots.ChoreSnapshot;

import java.util.List;


@AllArgsConstructor
@Getter
public class ChoreAllResponse {

    private List<ChoreSnapshot> chores;

}
