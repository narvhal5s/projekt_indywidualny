import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-error-view',
  templateUrl: './error-view.component.html',
  styleUrls: ['./error-view.component.css']
})
export class ErrorViewComponent implements OnInit {
  code:String;
  message:String;
  constructor(
    private route:ActivatedRoute
  ) { }

  ngOnInit() {
    this.code = this.route.snapshot.paramMap.get("code");
    this.message = this.route.snapshot.paramMap.get("message");
  }

}
