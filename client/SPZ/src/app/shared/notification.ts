export class notification{
    id:number;
    type:number;
    message:String;

    constructor(type:number,message:String){
        this.type = type;
        this.message = message;
    }
}