export class UserChore{

    constructor(
    public id:number,
    public name:String,
    public time:number,
    public difficult: E_DIFFICULT,
    public type: E_TYPE,
    public details:String = "",
    public surveyStatus:boolean,
    public fit?:number,
    public done?:number,
    ){}
}