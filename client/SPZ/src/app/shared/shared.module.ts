import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule} from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NotificationComponent } from './notification/notification.component';
import { ErrorViewComponent } from './error-view/error-view.component';

@NgModule({
  declarations: [
    NotificationComponent,
    ErrorViewComponent
  ],
  imports: [
    CommonModule,
  ],
  exports:[
    CommonModule,
    FormsModule,
    BrowserModule,
    NotificationComponent
  ]
})
export class SharedModule { }
