import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { LoginForm } from '../shared/LoginForm';
import { RegisterForm } from '../shared/RegisterForm';
import { JwtResponse } from '../shared/Jwt-response';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' , observe: 'response' })
};

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {
  httpLink:String = "http://localhost:8080/api/auth"
  loginForm:LoginForm;
  constructor(private http:HttpClient) { }

  login(loginForm:LoginForm): Observable<JwtResponse> {
    return this.http.post<JwtResponse>(this.httpLink +"/signin", loginForm, httpOptions);
  }
 
  register(sigupForm:RegisterForm): Observable<string> {
    return this.http.post<string>(this.httpLink + "/signup", sigupForm, httpOptions);
  }

  getUserData():Observable<any>{
    return this.http.get<any>(this.httpLink + "/user",httpOptions);
  }
  putUserData(userInfo:any):Observable<any>{
    return this.http.put<any>(this.httpLink + "/modifyuser", userInfo,httpOptions)
  }
}
