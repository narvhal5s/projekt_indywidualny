import { Injectable } from '@angular/core';
import { notification } from '../shared/notification';
import { Observable,of } from 'rxjs';
import { NotificationComponent } from '../shared/notification/notification.component';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {
  private notify:notification[] = [];
  constructor() { }

  addNotification(type:number,message:String):void{
    console.log(type);
    console.log(message);
    this.notify.push(new notification(type,message));
  }

  deleteNotification(notification:notification){
    const index = this.notify.indexOf(notification);
    this.notify.splice(index,1);
  }

  getNotification():Observable<notification[]>{
    return of(this.notify);
  }

}
