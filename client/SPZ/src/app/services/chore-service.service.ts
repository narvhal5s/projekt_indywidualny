import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Chore } from '../shared/chore';
import { Observable } from 'rxjs';
import { randomChoreForm } from '../shared/randomChoreForm';
import { UserChore } from '../shared/UserChore';
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json'})
};
@Injectable({
  providedIn: 'root'
})
export class ChoreService {
  httpLink:string="http://localhost:8080/api/chore";
  constructor(private http:HttpClient) { }


  addChore( chore:Chore): Observable<number> {
   return this.http.post<number>( this.httpLink + "/newchore",chore, httpOptions);
  }

  addUserChore(ida:number): Observable<any>{
    let request:any = {};
    request.choreID=ida;
    return this.http.post<any>(this.httpLink + "/userchore", request , httpOptions);
  }

  getChores (): Observable<Chore[]> {
    return this.http.get<Chore[]>( this.httpLink + "/allchore",httpOptions);
  }

  getRandomChoreId(form:randomChoreForm) : Observable<number> {
    return this.http.get<number>(this.httpLink + "/randomchore",httpOptions);
  }

  getChore(id:number):Observable<Chore> {
    return this.http.get<Chore>(this.httpLink + "/"+id,httpOptions);
  }

  getUserChores():Observable<UserChore[]>{
    return this.http.get<UserChore[]>(this.httpLink + "/userchore",httpOptions);
  }

  getUserChore(id:number):Observable<UserChore>{
    return this.http.get<UserChore>(this.httpLink + "/userchore/"+id,httpOptions);
  }
  putUserChoreDetails(chore:UserChore):Observable<any>{
    let request:any = {};
    request.id = chore.id;
    request.done = chore.done;
    request.fit = chore.fit;
    return this.http.put(this.httpLink + "/putUserChore",request,httpOptions);
  }

}
