import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ErrorViewComponent } from './shared/error-view/error-view.component';

const routes: Routes = [
  {
    path:'error/:code/:message',
    component: ErrorViewComponent
  },  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
