import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ChoreModule } from './chore/chore.module';
import { UserModule } from './user/user.module';
import { HistoryModule } from './history/history.module'
import { FormsModule } from '@angular/forms';
import { httpInterceptorProviders } from './services/AuthInetrceptor';
import { SharedModule } from './shared/shared.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ChoreModule,
    UserModule,
    HistoryModule,
    HttpClientModule,
    FormsModule,
    SharedModule
  ],
  providers: [httpInterceptorProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
