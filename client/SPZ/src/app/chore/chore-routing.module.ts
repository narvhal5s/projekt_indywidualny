import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddChoreComponent } from './add-chore/add-chore.component';
import { RandomChoreFormComponent } from './random-chore-form/random-chore-form.component';
import { ChoresListComponent } from './chores-list/chores-list.component';
import { ChoreViewComponent } from './chore-view/chore-view.component';
import { ChoreDetailsComponent } from './chore-details/chore-details.component';

const routes: Routes = [
  {
    path:'chores/add',
    component: AddChoreComponent
  },
  {
    path:'chores/random',
    component: RandomChoreFormComponent
  },
  {
    path:'chores/list',
    component: ChoresListComponent
  },  
  {
    path:'choredetails/:id',
    component:ChoreDetailsComponent
  },
  {
    path:'chore/:id',
    component:ChoreViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ChoreRoutingModule { }
