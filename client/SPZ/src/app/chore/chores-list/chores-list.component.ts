import { Component, OnInit } from '@angular/core';
import { ChoreService } from 'src/app/services/chore-service.service';
import { Chore } from 'src/app/shared/chore';
import { Router } from '@angular/router';

@Component({
  selector: 'app-chores-list',
  templateUrl: './chores-list.component.html',
  styleUrls: ['./chores-list.component.css']
})
export class ChoresListComponent implements OnInit {
  choresList:Chore[]
  constructor(
    private choreService:ChoreService,
    public router:Router
    ) { }

  ngOnInit() {
    this.choreService.getChores().subscribe(data => {this.choresList = data;},
      error =>(this.router.navigate(['error/:code/:message',{code:error.status,message:error.error.error}]))  );
  }

}
