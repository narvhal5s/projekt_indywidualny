import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Chore } from 'src/app/shared/chore';
import { ChoreService } from 'src/app/services/chore-service.service';

@Component({
  selector: 'app-chore-details',
  templateUrl: './chore-details.component.html',
  styleUrls: ['./chore-details.component.css']
})
export class ChoreDetailsComponent implements OnInit {
  chore:Chore;
  constructor(
    private route:ActivatedRoute,
    public router:Router,
    private choreService:ChoreService  
  ) { }

  ngOnInit() {
      let id;
      id = this.route.snapshot.paramMap.get("id");
      this.choreService.getChore(id).subscribe(data =>{this.chore=data},
        error =>(this.router.navigate(['error/:code/:message',{code:error.status,message:error.error.error}])));
  }

}
