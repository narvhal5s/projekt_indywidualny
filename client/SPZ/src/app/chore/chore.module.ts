import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';

import { ChoreRoutingModule } from './chore-routing.module';
import { AddChoreComponent } from './add-chore/add-chore.component';
import { RandomChoreFormComponent } from './random-chore-form/random-chore-form.component';
import { ChoresListComponent } from './chores-list/chores-list.component';
import { httpInterceptorProviders } from '../services/AuthInetrceptor';
import { ChoreViewComponent } from './chore-view/chore-view.component';
import { ChoreDetailsComponent } from './chore-details/chore-details.component';

@NgModule({
  declarations: [
    AddChoreComponent, 
    RandomChoreFormComponent, ChoresListComponent, ChoreViewComponent, ChoreDetailsComponent
  ],
  imports: [
    SharedModule,
    ChoreRoutingModule,
  ],
  providers:[
    httpInterceptorProviders
  ]
})
export class ChoreModule { }
