import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RandomChoreFormComponent } from './random-chore-form.component';

describe('RandomChoreFormComponent', () => {
  let component: RandomChoreFormComponent;
  let fixture: ComponentFixture<RandomChoreFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RandomChoreFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RandomChoreFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
