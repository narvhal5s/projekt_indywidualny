import { Component, OnInit } from '@angular/core';
import { randomChoreForm } from 'src/app/shared/randomChoreForm';
import { ChoreService } from 'src/app/services/chore-service.service';
import { Chore } from 'src/app/shared/chore';
import { Router } from '@angular/router';
import { ThrowStmt } from '@angular/compiler';

@Component({
  selector: 'app-random-chore-form',
  templateUrl: './random-chore-form.component.html',
  styleUrls: ['./random-chore-form.component.css']
})
export class RandomChoreFormComponent implements OnInit {
  model = new randomChoreForm(null,null,null);
  id:number=5;
  constructor(private choreService:ChoreService, public router:Router) {
  }

  ngOnInit() {
  }
  confirm(){
    this.choreService.getRandomChoreId(this.model).subscribe(data =>{
      this.router.navigate(['chore/:id',{id:data}]);},
      error =>(this.router.navigate(['error/:code/:message',{code:error.status,message:error.error.error}])));
  }
  move(){
  }
}
