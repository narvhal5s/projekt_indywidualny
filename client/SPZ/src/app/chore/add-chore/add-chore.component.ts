import { Component, OnInit } from '@angular/core';
import { Chore } from 'src/app/shared/chore';
import { ChoreService } from 'src/app/services/chore-service.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-add-chore',
  templateUrl: './add-chore.component.html',
  styleUrls: ['./add-chore.component.css']
})
export class AddChoreComponent implements OnInit {
  added:boolean;
  model:Chore = new Chore(null,null,null,null,null,null);
  timevalid = true;
  code:number;
  constructor(
    private choresService:ChoreService,
    public router:Router
    ) {}


  ngOnInit() {
  }

  submitted = false;

  onSubmit(){
    this.submitted = true;
  }

  confirm(){
    this.choresService.addChore(this.model).subscribe(result => {this.added=true},
      error =>(this.router.navigate(['error/:code/:message',{code:error.status,message:error.error.error}])));
  }


  timeCheck():void{
    if(this.model.time < 0 || this.model.time > 1440){
      this.timevalid = false;
      return;
    }
    this.timevalid = true;
  }

  nextOne(){
    this.added = false;
  }
}
