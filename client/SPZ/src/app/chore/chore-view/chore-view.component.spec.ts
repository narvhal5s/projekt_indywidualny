import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChoreViewComponent } from './chore-view.component';

describe('ChoreViewComponent', () => {
  let component: ChoreViewComponent;
  let fixture: ComponentFixture<ChoreViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChoreViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChoreViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
