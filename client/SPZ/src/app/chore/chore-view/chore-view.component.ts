import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { ChoreService } from 'src/app/services/chore-service.service';
import { Chore } from 'src/app/shared/chore';
import { NotificationService } from 'src/app/services/notification.service';

@Component({
  selector: 'app-chore-view',
  templateUrl: './chore-view.component.html',
  styleUrls: ['./chore-view.component.css']
})
export class ChoreViewComponent implements OnInit {
  chore:Chore;
  constructor(
    private route:ActivatedRoute,
    public router:Router,
    private choreService:ChoreService,
    private notificationService:NotificationService
  ) { }

  ngOnInit() {
      let id;
      id = this.route.snapshot.paramMap.get("id");
      this.choreService.getChore(id).subscribe(data =>{this.chore=data},
        error => {this.notificationService.addNotification(error.error.status,error.error.message)});
  }

  acceptChore(){
    this.choreService.addUserChore(this.chore.id).subscribe(data =>(this.router.navigate(['history/userchores'])));
  }

}
