import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChoreSurveyComponent } from './chore-survey.component';

describe('ChoreSurveyComponent', () => {
  let component: ChoreSurveyComponent;
  let fixture: ComponentFixture<ChoreSurveyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChoreSurveyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChoreSurveyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
