import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompletedChoreDetailComponent } from './completed-chore-detail.component';

describe('CompletedChoreDetailComponent', () => {
  let component: CompletedChoreDetailComponent;
  let fixture: ComponentFixture<CompletedChoreDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompletedChoreDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompletedChoreDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
