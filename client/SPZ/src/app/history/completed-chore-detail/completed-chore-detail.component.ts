import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ChoreService } from 'src/app/services/chore-service.service';
import { UserChore } from 'src/app/shared/UserChore';

@Component({
  selector: 'app-completed-chore-detail',
  templateUrl: './completed-chore-detail.component.html',
  styleUrls: ['./completed-chore-detail.component.css']
})
export class CompletedChoreDetailComponent implements OnInit {
  chore:UserChore;
  editing:boolean=false;
  constructor(
    private route:ActivatedRoute,
    public router:Router,
    private choreService:ChoreService ) { }

  ngOnInit() {
    let id;
    id = this.route.snapshot.paramMap.get("id");
    this.choreService.getUserChore(id).subscribe(data =>{this.chore=data});
  }

  confirm(){
    this.editing = false;
    this.choreService.putUserChoreDetails(this.chore).subscribe(
      data =>{},
      error =>(this.router.navigate(['error/:code/:message',{code:error.status,message:error.error.error}]))
    )
    }
  
  edit(){
    this.editing = true;
  }

}
