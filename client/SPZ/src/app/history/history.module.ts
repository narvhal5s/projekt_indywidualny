import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HistoryRoutingModule } from './history-routing.module';
import { ChoreSurveyComponent } from './chore-survey/chore-survey.component';
import { CompletedChoresComponent } from './completed-chores/completed-chores.component';
import { CompletedChoreDetailComponent } from './completed-chore-detail/completed-chore-detail.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [ChoreSurveyComponent, CompletedChoresComponent, CompletedChoreDetailComponent],
  imports: [
    CommonModule,
    HistoryRoutingModule,
    SharedModule
  ]
})
export class HistoryModule { }
