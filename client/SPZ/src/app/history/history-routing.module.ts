import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChoreSurveyComponent } from './chore-survey/chore-survey.component';
import { CompletedChoresComponent } from './completed-chores/completed-chores.component';
import { CompletedChoreDetailComponent } from './completed-chore-detail/completed-chore-detail.component';

const routes: Routes = [
  {
    path:'history/choresurvey',
    component: ChoreSurveyComponent
  },  
  {
    path:'history/userchores',
    component: CompletedChoresComponent
  },
  {
    path:'history/userchore/:id',
    component: CompletedChoreDetailComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HistoryRoutingModule { }
