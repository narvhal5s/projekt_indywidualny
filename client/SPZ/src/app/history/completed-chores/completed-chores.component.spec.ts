import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompletedChoresComponent } from './completed-chores.component';

describe('CompletedChoresComponent', () => {
  let component: CompletedChoresComponent;
  let fixture: ComponentFixture<CompletedChoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompletedChoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompletedChoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
