import { Component, OnInit } from '@angular/core';
import { ChoreService } from 'src/app/services/chore-service.service';
import { UserChore } from 'src/app/shared/UserChore';
import { NotificationService } from 'src/app/services/notification.service';

@Component({
  selector: 'app-completed-chores',
  templateUrl: './completed-chores.component.html',
  styleUrls: ['./completed-chores.component.css']
})
export class CompletedChoresComponent implements OnInit {
  userChores:UserChore[];
  constructor(private choreService:ChoreService,
              private notificationService:NotificationService) { }

  ngOnInit() {
    this.choreService.getUserChores().subscribe(data => {this.userChores = data},
      error => {this.notificationService.addNotification(error.status,error.message)}
      )
  }

}
