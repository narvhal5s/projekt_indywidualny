import { Component, OnInit } from '@angular/core';
import { RegisterForm } from 'src/app/shared/RegisterForm';
import { UserServiceService } from 'src/app/services/user-service.service';
import { NotificationService } from 'src/app/services/notification.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sing-up-user',
  templateUrl: './sing-up-user.component.html',
  styleUrls: ['./sing-up-user.component.css']
})
export class SingUpUserComponent implements OnInit {
  form:RegisterForm;
  passwordConf:string = "";
  error:string;
  passSame:boolean = true;
  sexNumber:number = 0;
  constructor(private userService:UserServiceService,
    private notificationService:NotificationService,
    public router:Router) { }

  ngOnInit() {
    this.form = new RegisterForm();
    this.error="";
  }

  confirm(){
    this.form.name = this.form.username;
    if(this.sexNumber == 1){
      this.form.sex = "Kobieta";
    }
    else if(this.sexNumber == 2){
      this.form.sex = "Mężczyzna";
    }
    else
      this.form.sex = "Inna";
    
    this.userService.register(this.form).subscribe(result => (this.router.navigate(['error/:code/:message',{code:"",message:"Zostałeś zalogowany"}])),
      error =>(this.router.navigate(['error/:code/:message',{code:error.status,message:error.error.error}])));
  }

}
