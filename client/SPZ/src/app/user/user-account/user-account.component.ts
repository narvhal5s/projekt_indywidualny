import { Component, OnInit } from '@angular/core';
import { UserServiceService } from 'src/app/services/user-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-account',
  templateUrl: './user-account.component.html',
  styleUrls: ['./user-account.component.css']
})
export class UserAccountComponent implements OnInit {
  userData:any=null;
  error:any;
  constructor(
    private userService:UserServiceService,
    public router:Router) { }

  ngOnInit() {
    this.userService.getUserData().subscribe(data => {this.userData = data},
      error =>(this.router.navigate(['error/:code/:message',{code:error.status,message:error.error.error}])))
  }

  click(){
    this.userService.putUserData(this.userData).subscribe(info =>{},
      error =>(this.router.navigate(['error/:code/:message',{code:error.status,message:error.error.error}])))
  }

}
