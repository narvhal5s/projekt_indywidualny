import { Component, OnInit } from '@angular/core';
import { LoginForm } from 'src/app/shared/LoginForm';
import { UserServiceService } from 'src/app/services/user-service.service';
import {TokenStorageService} from '../../services/token-storage.service'
@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {
  model: any = {};
  isLoggedIn = false;
  isLoginFailed = false;
  username: string = "";
  errorMessage = '';
  roles: string[] = [];
  private loginInfo: LoginForm;

  constructor(private authService: UserServiceService, private tokenStorage: TokenStorageService) { }

  ngOnInit() {
    if (this.tokenStorage.getToken()) {
      this.isLoggedIn = true;
      this.roles = this.tokenStorage.getAuthorities();
      this.username = this.tokenStorage.getUsername();
    }
  }

  confirm() {
    console.log(this.model);

    this.loginInfo = new LoginForm(
      this.model.username,
      this.model.password);

    this.authService.login(this.loginInfo).subscribe(
      data => {
        this.tokenStorage.saveToken(data.token);
        this.tokenStorage.saveUsername(data.username);
        this.tokenStorage.saveAuthorities(data.authorities);

        this.isLoginFailed = false;
        this.isLoggedIn = true;
        this.roles = this.tokenStorage.getAuthorities();
        this.reloadPage();
      },
      error => {
        console.log(error);
        this.errorMessage = error.error.message;
        this.isLoginFailed = true;
      }
    );
  }
  reloadPage() {
    window.location.reload();
  }
  logout(){
    this.tokenStorage.signOut();
    this.reloadPage();
  }
}
