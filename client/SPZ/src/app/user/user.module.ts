import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';

import { UserRoutingModule } from './user-routing.module';
import { SingUpUserComponent } from './sing-up-user/sing-up-user.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { UserAccountComponent } from './user-account/user-account.component';
import { httpInterceptorProviders } from '../services/AuthInetrceptor';

@NgModule({
  declarations: [SingUpUserComponent, LoginFormComponent, UserAccountComponent],
  imports: [
    SharedModule,
    UserRoutingModule,
  ],
  exports:[
    LoginFormComponent,
    UserRoutingModule
  ],
  providers:[
    httpInterceptorProviders
  ]
})
export class UserModule { }
