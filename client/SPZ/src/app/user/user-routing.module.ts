import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SingUpUserComponent } from './sing-up-user/sing-up-user.component';
import { UserAccountComponent } from './user-account/user-account.component';
import { LoginFormComponent } from './login-form/login-form.component';

const routes: Routes = [
  {
    path:'user/signup',
    component: SingUpUserComponent,
  },  
  {
    path:'user/signin',
    component: LoginFormComponent,
  },  
  {
    path:'user/account',
    component: UserAccountComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UserRoutingModule { }
